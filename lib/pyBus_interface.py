import serial, time, logging

# LOCATIONS, a mapping of hex codes seen in SRC/DST parts of packets. This WILL change across models/years.
LOCATIONS = {
  '00' : 'Broadcast',
  '18' : 'CDW - CDC CD-Player',
  '30' : '?????',
  '3B' : 'NAV Navigation/Videomodule',
  '43' : 'MenuScreen',
  '44' : 'Ignition?',
  '50' : 'MFL Multi Functional Steering Wheel Buttons',
  '60' : 'PDC Park Distance Control',
  '68' : 'RAD Radio',
  '6A' : 'DSP Digital Sound Processor',
  '7F' : '?????',
  '80' : 'IKE Instrument Control Electronics',
  'BF' : 'BROADCAST LCM?',
  'C0' : 'MID Multi-Information Display Buttons',
  'C8' : 'TEL Telephone',
  'D0' : 'Navigation Location',
  'E7' : 'OBC TextBar',
  'E8' : '?????',
  'ED' : 'Lights, Wipers, Seat Memory',
  'F0' : 'BMB Board Monitor Buttons',
  'FF' : 'Broadcast'
}

#------------------------------------
# CLASS for iBus communications
#------------------------------------
class ibusFace ( ):
  # Initialize the serial connection - then use some commands I saw somewhere once
  def __init__(self, devPath,):
    self.SDEV = serial.Serial(
      devPath,
      baudrate=9600,
      bytesize=serial.EIGHTBITS,
      parity=serial.PARITY_EVEN,
      stopbits=serial.STOPBITS_ONE,
      timeout=2
    )
    self.SDEV.setDTR(True)
    self.SDEV.flushInput()
    self.SDEV.lastWrite = int(round(time.time() * 1000))
    self.PACKET_STACK = []
    self.nodes = {
      "CD" : 0x18,
      "RADIO" : 0x68,
      "BROADCAST" : 0xFF,
    }
    logging.debug("Initialized iBus")

  class packet():
    def __init__(self, src = None, length = None, dst = None, dat = [], xor = None):
      self.fields = {
        "src" : src,
        "len" : length,
        "dst" : dst,
        "dat" : dat,
        "xor" : xor
      }

    # get the checksum of a complete packet to be appended to the packet - I think everything listening on ibus checks these packets (except for this tool)
    def getCheckSum(self):
      #backup dat field
      dataOrig = self.fields["dat"]
      #calculate the checksum of the dat field, make dat that value so it can be checked with everything else 
      dataCheck = 0
      for d in self.fields["dat"]:
        dataCheck ^= d
      self.fields["dat"] = dataCheck
      #calculate total checksum
      chk = 0
      self.fields["xor"] = 0
      for p in self.fields.values():
        chk ^= p
      #set dat field back
      self.fields["dat"] = dataOrig
      #set the xor field
      self.fields["xor"] = chk
      return chk

    def getLength(self):
      packetLength = len(self.fields["dat"]) + 2 #length of packet without sourceID and length itself
      self.fields["len"] = packetLength
      return packetLength

    def pack(self):
      self.getLength()
      self.getCheckSum()

    def __eq__(self, other):
      return ((self.fields["src"] == other.fields["src"]) &
             (self.fields["dst"] == other.fields["dst"]) &
             (self.fields["len"] == other.fields["len"]) &
             (self.fields["dat"] == other.fields["dat"]))
    def __str__(self):
      retString = "" + hex(self.fields["src"]) + " " + hex(self.fields["len"]) + " " + hex(self.fields["dst"])
      for data in self.fields["dat"]:
        retString = retString +  " " + hex(data)  
      retString = retString +  " " + hex(self.fields["xor"])
      return retString
    def __repr__(self):
      return self.__str__()

  # Wait for a significant delay in the bus before parsing stuff (signals separated by pauses)
  def waitClearBus(self):
    logging.debug("Waiting for clear bus")
    oldTime = time.time()
    while True:
      # Wait for large interval between packets
      swallow_char = self.readChar() # will be src packet if there was a significant delay between packets. Otherwise its nothing useful
      newTime = time.time()
      deltaTime = newTime - oldTime
      oldTime = newTime
      if deltaTime > 0.1:
        break # we have found a significant delay in signals, but have swallowed the first character in doing so.
              # So the next code swallows what should be the rest of the packet

    packetLength = self.readChar() # len packet
    self.readChar() # dst packet
    dataLen = int(packetLength, 16) - 2 # Determind length of this packet from the packetLength variable, then swallow that
    while dataLen > 0:
      self.readChar()
      dataLen = dataLen - 1
    self.readChar() # XOR packet. This will be the last bit of the packet. I could change the while loop variable by one, but this adds clarity.

  # Read a packet from the bus
  def readBusPacket(self):
    try:
    # packet = {
    #   "src" : None,
    #   "len" : None,
    #   "dst" : None,
    #   "dat" : [],
    #   "xor" : None
    # }
      packet = self.packet()
      packet.fields["src"] = self.readChar()
      packet.fields["len"] = self.readChar()
      packet.fields["dst"] = self.readChar()

      dataLen = packet.fields['len']- 2
      if dataLen > 20:
        logging.critical("Length of +20 found, no useful packet is this long.. cleaning up")
        self.waitClearBus()
        return None

      dataTmp = []
      while dataLen > 0:
        dataTmp.append(self.readChar())
        dataLen = dataLen - 1
      packet.fields['dat'] = dataTmp
      packet.fields['xor'] = self.readChar()
      
      valStr = [packet.fields['src'], packet.fields['len'], packet.fields['dst'], packet.fields['dat'], packet.fields['xor']]
      logging.debug("READ: %s" % valStr)

      print "Read from bus: ",
      print packet
      return packet
    except serial.SerialException:
      print "exception in readBusPacket"
      pass
  # Read in one character from the bus and convert to hex
  def readChar(self):
    char = None
    try:
      char = self.SDEV.read(1)
    except serial.SerialException, e: 
      logging.warning("Hit a serialException: %s" % e)
      print "Hit serialException"
      pass
    if (len(char) != 0):
      char = ord(char)
    return char

  # Write a string of data created from complete contents of packet
  def writeFullPacket(self, packet):
    #backup the dat field back
    dataOrig = packet.fields["dat"]
    #squash the dat field into a string
    #packet.fields["dat"] = ''.join(chr(d) for d in packet.fields["dat"])
    #print packet.fields["dat"]
    #squash the whole packet into a string
    #data = ''.join(chr(p) for p in packet.fields.values())

    dataToWrite = [packet.fields["src"], packet.fields["len"], packet.fields["dst"]]
    dataToWrite.extend(packet.fields["dat"])
    dataToWrite.append(packet.fields["xor"])
    print "Writing to bus: ",
    print packet

    self.SDEV.write(dataToWrite)

    self.SDEV.flush()
    #put the dat field back
    packet.fields["dat"] = dataOrig

  # Write Packet to iBus, first length is determined, the packet is then constructed and a checksum generated/appended.
  # The packet is then sent if the CTS signal is good (Clear To Send)
  # TODO: Read to verify the packet we send is seen
  def writeBusPacket(self, src = None, dst = None, data = None, packet = None):
    time.sleep(0.01) # pause a tick

    if (src != None):
      #length = (2 + len(data))
      packet = self.packet(src = src, dst = dst, dat = data)

    packet.pack()

    packetSent = False
    while (not packetSent):
      logging.debug("WRITE: %s" % packet)
      if (self.SDEV.getCTS()) and ((int(round(time.time() * 1000)) - self.SDEV.lastWrite) > 10): # dont write packets to close together.. issues arise
        self.writeFullPacket(packet)
        logging.debug("WRITE: SUCCESS")
        self.SDEV.lastWrite = int(round(time.time() * 1000))
        packetSent = True
      else:
        logging.debug("WRITE: WAIT")
        time.sleep(0.01)

  def close(self):
    self.SDEV.close()
#---------- END CLASS -------------
